import random as r

r.seed(2023)

def ventasDelMes(t):
	random = r.uniform(-1.0, 1.0)
	sales = 3*t + 2*random
	roundedSales = round(sales)
	return roundedSales

for i in range(1,13): #Simulación para las ventas de los primeros 12 meses
	print("Mes " + str(i) + ": " + str(ventasDelMes(i)))


# Simulación del mes 13
print("-------------")
print("Mes 13: " + str(ventasDelMes(13)))
