x = [1,2,3,4,5,6,7,8,9,10,11,12] #Meses de ventas
y = [3,8,10,11,15,17,19,22,26,31,34,35] #Ventas por mes
n = 12 #Observaciones


def fProm(valores):
	sumatoria = 0
	for i in range(len(valores)):
		sumatoria += valores[i]
	return (sumatoria/len(valores))

xProm = fProm(x)
print("Valor medio X = " + str(xProm))

yProm = fProm(y)
print("Valor medio Y = " + str(yProm))


def fB1(x, y, xProm, yProm, n):
	arriba = 0
	abajo = 0
	for i in range(n):
		arriba += (x[i]*y[i])
		abajo += ((x[i]-xProm)**2)
	arriba += (- n*xProm*yProm)
	return (arriba/abajo)

b1 = fB1(x, y, xProm, yProm, n)
print("B1 = " + str(b1))

b0 = yProm- b1*xProm
print("B0 = " + str(b0))


def fRegresionLineal(x, y, xProm, yProm, n):
	arriba = 0
	abajoIzq = 0
	abajoDer = 0
	for i in range(n):
		arriba += (x[i]-xProm)*(y[i]-yProm)
		abajoIzq += ((x[i]-xProm)**2)
		abajoDer += ((y[i]-yProm)**2)
	abajoIzq = abajoIzq**(1/2)
	abajoDer = abajoDer**(1/2)
	abajo = abajoIzq*abajoDer
	return (arriba/abajo)

r = fRegresionLineal(x, y, xProm, yProm, n)
print("r = " + str(r))


def fYEsperado(x, b0, b1):
	return (b0+b1*x)

def fAsociacionLineal(x, y, xProm, yProm, n, b0, b1):
	arribaIzq = 0
	arribaDer = 0
	abajo = 0
	for i in range(n):
		arribaIzq += ((y[i]-yProm)**2)
		arribaDer += ((y[i]-fYEsperado(x[i],b0,b1))**2)
		abajo += ((y[i]-yProm)**2)
	arriba = arribaIzq - arribaDer
	return (arriba/abajo)

r2 = fAsociacionLineal(x, y, xProm, yProm, n, b0, b1)
print("R2 = " + str(r2))

def fAsociacionLinealAjustado(r2, n):
	return (r2-((1-r2)/(n-2)))

r2a = fAsociacionLinealAjustado(r2, n)
print("R2a = " + str(r2a))


def fSST(y, yProm, n):
	res = 0
	for i in range(n):
		res += ((y[i]-yProm)**2)
	return res

sst = fSST(y, yProm, n)
print("SST = " + str(sst))


def fSSR(x, y, xProm, yProm, n, b0, b1):
	res = 0
	for i in range(n):
		res += ((fYEsperado(x[i], b0, b1) - yProm)**2)
	return res

ssr = fSSR(x, y, xProm, yProm, n, b0, b1)
print("SSR = " + str(ssr))


sse = sst - ssr
print("SSE = " + str(sse))


mse = sse/(n-2)
print("MSE = " + str(mse))


def fSE(x, xProm, n, mse):
	abajo = 0
	for i in range(n):
		abajo += ((x[i]-xProm)**2)
	res = ((mse/abajo)**(1/2))
	return res

se = fSE(x, xProm, n, mse)
print("SE = " + str(se))


estimado13 = fYEsperado(13, b0, b1)
print("Estimado para el mes 13 = " + str(estimado13))


f = (ssr/mse)
print("F = " + str(f))
